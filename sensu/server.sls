/root/sensu:
  file.recurse:
    - source: salt://sensu/files/sensu

# Was necessary replace multi lines, this is not the best way, but works.
# file.blockreplace is a workaround
# In the next saltstack version, file.replace will support multi lines
replace_rabbitmq_ip:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<sensu-rabbitmq-ip>'
    - repl: {{ pillar.get('server_ip') }}

replace_rabbitmq_user:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<rabbitmq-user>'
    - repl: {{ pillar.get('rabbitmq_user') }}

replace_rabbitmq_pass:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<rabbitmq-pass>'
    - repl: {{ pillar.get('rabbitmq_pass') }}

replace_rabbitmq_vhost:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<rabbitmq-vhost>'
    - repl: {{ pillar.get('rabbitmq_vhost') }}

replace_redis_ip:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<sensu-redis-ip>'
    - repl: {{ pillar.get('server_ip') }}

replace_sensu_ip:
  file.replace:
    - name: /root/sensu/config.json
    - pattern: '<sensu-server-ip>'
    - repl: {{ pillar.get('server_ip') }}

build_server_image:
  cmd.run:
    - name: docker build -t {{ pillar.get('sensu_image') }} /root/sensu/

run_server_container:
  dockerng.running:
    - image: {{ pillar.get('sensu_image') }}
    - name: {{ pillar.get('sensu_contname') }}
    - hostname: {{ pillar.get('sensu_contname') }}
    - restart_policy: always
    - port_bindings:
      - 4567:4567


