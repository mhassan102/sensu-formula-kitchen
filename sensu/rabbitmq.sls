get_rabbitmq_image:
  cmd.run:
    - name: docker pull {{ pillar.get('rabbitmq_image') }}

run_rabbitmq_container:
  dockerng.running:
    - image: {{ pillar.get('rabbitmq_image') }}
    - name: {{ pillar.get('rabbitmq_contname') }}
    - hostname: {{ pillar.get('rabbitmq_contname') }}
    - restart_policy: always
    - port_bindings:
      - 4369:4369
      - 5671:5671
      - 5672:5672
      - 15671:15671
      - 15672:15672
      - 25672:25672
    - environment:
      - RABBITMQ_DEFAULT_USER: {{ pillar.get('rabbitmq_user') }}
      - RABBITMQ_DEFAULT_PASS: {{ pillar.get('rabbitmq_pass') }}
      - RABBITMQ_DEFAULT_VHOST: {{ pillar.get('rabbitmq_vhost') }}
