get_redis_image:
  cmd.run:
    - name: docker pull {{ pillar.get('redis_image') }}

# It's important to store the data in another volume
create_redis_datavolume:
  cmd.run:
    - name: docker create -v /data --name {{ pillar.get('redis_dv') }} {{ pillar.get('redis_image') }}

run_redis_container:
  dockerng.running:
    - image: {{ pillar.get('redis_image') }}
    - name: {{ pillar.get('redis_contname') }}
    - hostname: {{ pillar.get('redis_contname') }}
    - restart_policy: always
    - port_bindings:
      - 6379:6379
    - volumes_from: {{ pillar.get('redis_dv') }}
