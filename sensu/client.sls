{% if grains['os_family'] == 'Debian' %}
python-apt:
  pkg:
    - installed
    - require_in:
      - pkgrepo: sensu
{% endif %}

sensu:
  pkgrepo.managed:
    - humanname: Sensu Repository
    {% if grains['os_family'] == 'Debian' %}
    - name: deb http://repositories.sensuapp.org/apt sensu main
    - file: /etc/apt/sources.list.d/sensu.list
    - key_url: http://repositories.sensuapp.org/apt/pubkey.gpg
    {% elif grains['os_family'] == 'RedHat' %}
    - baseurl: http://repositories.sensuapp.org/yum/el/$releasever/$basearch/
    - gpgcheck: 0
    - enabled: 1
    {% endif %}
    - require_in:
      - pkg: sensu
  pkg:
    - installed

/etc/sensu:
  file.recurse:
    - source: salt://sensu/files/client-sensu

# Was necessary replace multi lines, this is not the best way, but works.
# file.blockreplace is a workaround
# In the next saltstack version, file.replace will support multi lines
replace_rmq_user:
  file.replace:
    - name: /etc/sensu/config.json
    - pattern: '<rabbitmq-user>'
    - repl: {{ pillar.get('rabbitmq_user') }}

replace_rmq_pass:
  file.replace:
    - name: /etc/sensu/config.json
    - pattern: '<rabbitmq-pass>'
    - repl: {{ pillar.get('rabbitmq_pass') }}

replace_rmq_vhost:
  file.replace:
    - name: /etc/sensu/config.json
    - pattern: '<rabbitmq-vhost>'
    - repl: {{ pillar.get('rabbitmq_vhost') }}

replace_rmq_ip:
  file.replace:
    - name: /etc/sensu/config.json
    - pattern: '<rabbitmq-ip>'
    - repl: {{ pillar.get('server_ip') }}

replace_client_name:
  file.replace:
    - name: /etc/sensu/conf.d/client.json
    - pattern: '<client-hostname>'
    - repl: {{ grains['fqdn'] }}

replace_client_ip:
  file.replace:
    - name: /etc/sensu/conf.d/client.json
    - pattern: '<client-ip>'
    - repl: {{ grains['ip4_interfaces']['eth1'][0] }}

sensu-client:
  service.running:
    - enable: True
    - require:
      - file: /etc/sensu/conf.d/client.json
      - file: /etc/sensu/config.json
    - watch:
      - file: /etc/sensu/conf.d/*

/etc/default/sensu:
  file.replace:
    - pattern: 'EMBEDDED_RUBY=false'
    - repl: 'EMBEDDED_RUBY=true'
    - watch_in:
      - service: sensu-client

install_network_checks:
  cmd.run:
    - name: sensu-install -p network-checks

install_cpu_checks:
  cmd.run:
    - name: sensu-install -p cpu-checks

install_disk_checks:
  cmd.run:
    - name: sensu-install -p disk-checks

install_memory_checks:
  cmd.run:
    - name: sensu-install -p memory-checks

install_graphite_plugins:
    cmd.run:
    - name: sensu-install -p graphite

install_vmstats_plugins:
  cmd.run:
    - name: sensu-install -p vmstats

install_process_plugins:
  cmd.run:
    - name: sensu-install -p process-checks

install_load_plugins:
  cmd.run:
    - name: sensu-install -p load-checks

/etc/sensu/conf.d:
  file.recurse:
    - source: salt://sensu/files/client-sensu/checks

restart_sensu_client:
  cmd.run:
    - name: /etc/init.d/sensu-client restart
