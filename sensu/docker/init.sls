{% from 'sensu/docker/map.jinja' import docker with context %}
{% if grains.os_family == 'Debian' %}
include:
  - sensu.docker.debian
{% elif grains.os_family == 'RedHat' %}
include:
  - docker.redhat
{% elif grains.os_family == 'Suse' %}
include:
  - docker.suse
{% endif %}
