#!/bin/bash
set -e

/etc/init.d/sensu-server start
/etc/init.d/sensu-api start
