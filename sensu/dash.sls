get_dash_image:
  cmd.run:
    - name: docker pull {{ pillar.get('dash_image') }}

/root/dash-sensu:
  file.recurse:
    - source: salt://sensu/files/dash-sensu

/root/dash-sensu/config.json:
  file.replace:
    - pattern: '<sensu-server-ip>'
    - repl: {{ pillar.get('server_ip') }}

run_dash_container:
  dockerng.running:
    - image: {{ pillar.get('dash_image') }}
    - name: {{ pillar.get('dash_contname') }}
    - hostname: {{ pillar.get('dash_contname') }}
    - restart_policy: always
    - port_bindings:
      - 3000:3000
    - binds: /root/dash-sensu:/config
